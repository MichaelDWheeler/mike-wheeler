var express = require('express');
var mail = express.Router();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var mandrill = require('mandrill-api/mandrill');
var config = require('../mailkey.js');
var mandrill_client = new mandrill.Mandrill(config.key);

mail.post('/mail', jsonParser, function (req, res) {
    var message = {
        "html": "<p>" + req.body.message + "</p>",
        "subject": "*You have received a message from someone*",
        "from_email": config.from,
        "from_name": req.body.name,
        "to": [{
            "email": config.from,
            "name": config.name,
            "type": "to"
        }],
        "headers": {
            "Reply-To": req.body.email
        },
        "metadata": {
            "phone": req.body.phone
        }
    };

    var async = false;
    var ip_pool = null;
    var send_at = null;
    mandrill_client.messages.send({ "message": message, "async": async, "ip_pool": ip_pool, "send_at": send_at }, function (result) {
        res.sendStatus(200);
    }, function (e) {
        res.sendStatus(500);
        // Mandrill returns the error as an object with name and message keys
        console.log('An error occurred sending the mail: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
});


module.exports = mail;