(function () {
    var background = new Image();
    var ctx = document.getElementById('my-canvas').getContext('2d');
    var cH = 600;
    var cW = 555;
    var drifting = true;
    var engineSound = new Audio();
    var boomSound = new Audio();
    var laserSound = new Audio();


    engineSound.src = 'mp3/Engine.mp3';
    boomSound.src = 'mp3/Boom.mp3';
    laserSound.src = 'mp3/Pew.mp3';

    
    ctx.canvas.width = 1920
    ctx.canvas.height = 1080;
    ctx.clearRect(0, 0, cW, cH);

    background.src = "./stars-background.jpg";

    var randomColor = function () {
        var color =
            '#' + (function co(lor) {
                return (lor +=
                    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f'][Math.floor(Math.random() * 16)])
                    && (lor.length === 6) ? lor : co(lor);
            })('');
        return color;
    };

    var randomNumber = function (num) {
        var sum = Math.random() * num;
        return sum;
    }

    var initCanvas = function () {
        //*** Moving Background ***
        var Background = function () {
            this.x = 0;
            this.width = window.innerWidth;
            this.height = window.innerHeight;

            this.render = function () {
                ctx.drawImage(background, this.x--, 0);
                if (this.x <= -959) {
                    this.x = 0;
                };
            };
        };

        ctx.beginPath();
        var x = 300,
            y = 175,
            // Radii of the white glow.
            innerRadius = 5,
            outerRadius = 55,
            // Radius of the entire circle.
            radius = 50;

        var gradient = ctx.createRadialGradient(x, y, innerRadius, x, y, outerRadius);
        gradient.addColorStop(0.15, 'white');
        gradient.addColorStop(0.7, 'yellow');
        gradient.addColorStop(1, '#FFCC00');

        var Sun = function () {
            this.render = function () {
                ctx.beginPath();
                ctx.arc(x, y, radius, 0, 2 * Math.PI);
                ctx.fillStyle = gradient;
                ctx.fill();
            };
        };

        var Rocketship = function () {
            this.x = 0;
            this.y = 0;

            this.render = function () {
                ctx.beginPath();
                ctx.strokeStyle = "grey";
                ctx.fillStyle = "white";
                //rocket cylinder
                ctx.moveTo(this.x, this.y);
                ctx.lineTo(this.x + 100, this.y);

                //rocket nose
                ctx.quadraticCurveTo(this.x + 135, this.y, this.x + 150, this.y + 15);
                ctx.quadraticCurveTo(this.x + 135, this.y + 25, this.x + 100, this.y + 25);

                //bottom line of rocket cylinder
                ctx.lineTo(this.x, this.y + 25);
                ctx.closePath();
                ctx.stroke();
                ctx.fill();
                //end rocket cylinder

                //rocket window
                ctx.beginPath();
                ctx.arc(this.x + 120, this.y + 12, 7, 0, Math.PI * 2, false);
                ctx.strokeStyle = "blue";
                ctx.fillStyle = "aqua";
                ctx.lineWidth = "3";
                ctx.stroke();
                ctx.fill();

                //cylinder bands
                ctx.beginPath();
                ctx.strokeStyle = "black";
                ctx.moveTo(this.x + 100, this.y);
                ctx.quadraticCurveTo(this.x + 105, this.y + 10, this.x + 100, this.y + 25);
                ctx.moveTo(this.x + 80, this.y);
                ctx.quadraticCurveTo(this.x + 85, this.y + 10, this.x + 80, this.y + 25);
                ctx.fillStyle = "blue";
                ctx.fill();

                //top fin
                ctx.beginPath();
                ctx.fillStyle = "red";
                ctx.moveTo(this.x + 15, this.y);
                ctx.lineTo(this.x + 5, this.y - 20);
                ctx.lineTo(this.x + 25, this.y - 20);
                ctx.lineTo(this.x + 55, this.y);

                //bottom fin
                ctx.moveTo(this.x + 15, this.y + 25);
                ctx.lineTo(this.x + 5, this.y + 45);
                ctx.lineTo(this.x + 25, this.y + 45);
                ctx.lineTo(this.x + 55, this.y + 25);

                //middle fin
                ctx.moveTo(this.x + 15, this.y + 10);
                ctx.rect(this.x + 5, this.y + 10, 50, 5);

                //rocket exhaust
                ctx.moveTo(this.x, this.y + 5);
                ctx.lineTo(this.x - 7, this.y);
                ctx.lineTo(this.x - 7, this.y + 25);
                ctx.lineTo(this.x, this.y + 20);
                ctx.fill();
                ctx.stroke();
            };

            var countForOuterFlame = 0;

            this.flameOn = function () {
                if (engineSound !== undefined) {
                    engineSound.play();
                }
                drifting = false;
                //rocket flame (outer);

                if (countForOuterFlame % 5 === 0 || countForOuterFlame % 4 === 0 || countForOuterFlame % 3 === 0 || countForOuterFlame % 2 === 0) {
                    ctx.beginPath();
                    ctx.strokeStyle = "white"
                    ctx.fillStyle = "orange";
                    ctx.moveTo(this.x - 8, this.y + 2);
                    ctx.quadraticCurveTo(this.x - 20, this.y - 10, this.x - 35, this.y - 2);
                    ctx.quadraticCurveTo(this.x - 30, this.y, this.x - 25, this.y);
                    ctx.quadraticCurveTo(this.x - 50, this.y + 5, this.x - 70, this.y + 12);
                    ctx.quadraticCurveTo(this.x - 50, this.y + 20, this.x - 25, this.y + 24);
                    ctx.quadraticCurveTo(this.x - 30, this.y + 30, this.x - 35, this.y + 26);
                    ctx.quadraticCurveTo(this.x - 20, this.y + 40, this.x - 8, this.y + 24);
                    ctx.stroke();
                    ctx.fill();
                }

                // rocket flame (inner)
                ctx.beginPath();
                ctx.strokeStyle = "white";
                ctx.fillStyle = gradient;
                ctx.moveTo(this.x - 8, this.y + 8);
                ctx.quadraticCurveTo(this.x - 12, this.y - 2, this.x - 25, this.y + 5);
                ctx.quadraticCurveTo(this.x - 22, this.y + 5, this.x - 20, this.y + 2);
                ctx.quadraticCurveTo(this.x - 25, this.y + 10, this.x - 30, this.y + 12);
                ctx.quadraticCurveTo(this.x - 25, this.y + 15, this.x - 20, this.y + 17);
                ctx.quadraticCurveTo(this.x - 22, this.y + 20, this.x - 25, this.y + 19);
                ctx.quadraticCurveTo(this.x - 12, this.y + 27, this.x - 8, this.y + 17);
                ctx.fill();
                ctx.stroke();

                countForOuterFlame++;
            };

            this.drift = function (e) {
                if (drifting) {
                    if (this.x > 10) {
                        this.x -= 0.4;
                    }
                }
            };
        };

        var backgroundImage = new Background();
        var sun = new Sun();
        var rocketship = new Rocketship();
        var map = {};
        var score = 0;;
        var LaserBeams = function () {

            this.render = function () {
                ctx.beginPath();
                ctx.strokeStyle = "#F81FF5";
                ctx.moveTo(rocketship.x + 140, rocketship.y + 15);
                ctx.lineTo(cW, rocketship.y + 15);
                ctx.lineWidth = "5";
                ctx.stroke();
            };

            this.checkForHit = function () {
                for (var i = 0; i < ufoArray.length; i++) {
                    if (ufoArray[i].x > rocketship.x && ufoArray[i].y - 10 <= rocketship.y + 10 && ufoArray[i].y + 20 >= rocketship.y + 15) {
                        boomSound.play();
                        score += 1;
                        ufoArray.splice(i, 1);
                    }
                }
            }
        };

        var moveInDirection = function (x, y) {
            rocketship.x += x;
            rocketship.y += y;
        };

        var moveRocket = function () {
            switch (true) {
                //right arrow and up arrow
                case map[39] && map[38]:
                    moveInDirection(5, -5);
                    rocketship.flameOn();
                    break;
                //right arrow and down arrow
                case map[39] && map[40]:
                    moveInDirection(5, 5);
                    rocketship.flameOn();
                    break;
                //left arrow and up arrow
                case map[37] && map[38]:
                    moveInDirection(-5, -5);
                    break;
                //left arrow and down arrow
                case map[37] && map[40]:
                    moveInDirection(-5, 5);
                    break;
                //right arrow
                case map[39]:
                    moveInDirection(5, 0);
                    rocketship.flameOn();
                    break;
                //left arrow
                case map[37]:
                    moveInDirection(-5, 0);
                    rocketship.x -= 5;
                    break;
                //up arrow
                case map[38]:
                    moveInDirection(0, -5);
                    rocketship.y -= 5;
                    rocketship.flameOn();
                    break;
                //down arrow
                case map[40]:
                    moveInDirection(0, 5);
                    rocketship.flameOn();
                    break;

                //spacebar
                case map[32]:
                    var laser = new LaserBeams;
                    laser.render(rocketship.x, rocketship.y);
                    laser.checkForHit();
                    if (laserSound !== undefined) {
                        laserSound.play();
                    }
                    speedUpUFOs();
                    break;
            }
        };

        var UFO = function (x, y, color) {
            this.x = x;
            this.y = y;
            this.speed = 6;
            this.render = function () {
                ctx.beginPath();
                ctx.strokeStyle = "silver";
                ctx.lineWidth = "1";

                //dome
                ctx.fillStyle = color;
                ctx.arc(this.x + 30, this.y, 10, 0, Math.PI, true);
                ctx.fill();
                //end dome

                ctx.beginPath();
                ctx.lineTo(this.x + 40, this.y);
                ctx.lineTo(this.x + 60, this.y + 10);
                ctx.quadraticCurveTo(this.x + 30, this.y + 20, this.x, this.y + 10);
                ctx.lineTo(this.x + 20, this.y);
                ctx.fillStyle = "#4682b4";
                ctx.fill();
                ctx.stroke();
            };

            this.speedUp = function () {
                this.speed += randomNumber(2);
            };

            this.move = function () {
                this.x += randomNumber(this.speed) - (this.speed / 2);
                this.y += randomNumber(this.speed) - (this.speed / 2);

                switch (true) {
                    case this.x >= cW - 50: this.x -= this.speed;
                        break;
                    case this.x <= 50: this.x = + this.speed;
                        break;
                    case this.y >= cH - 20: this.y -= this.speed;
                        break;
                    case this.y <= 20: this.y += this.speed;
                        break
                }
            }
        };

        var ufoArray = [];

        var createUFOs = function (num) {
            var length = randomNumber(8 + num);
            if (length < 2) {
                length = 4;
            }
            for (var i = 0; i <= length; i++) {
                var xPos = randomNumber(cW);
                var yPos = randomNumber(cH);

                if (xPos >= rocketship.x - 150 && xPos <= rocketship.x + 150) {
                    xPos += xPos + 400;
                }

                ufoArray.push(new UFO(xPos, yPos, randomColor()));
            }
        };

        var renderUFOs = function () {
            for (var i = 0; i < ufoArray.length; i++) {
                ufoArray[i].render();
                ufoArray[i].move();
            }
        };

        var Star = function (x, y) {
            this.x = randomNumber(250) + x;
            this.y = randomNumber(150) + y;
            this.speed = randomNumber(20);
            this.dirX = randomNumber(25) - 12;
            this.dirY = randomNumber(25) - 12;
            this.timeLimit = randomNumber((6) + 2) * 1000;
            this.starLength = randomNumber(10);

            this.draw = function () {
                ctx.beginPath();
                ctx.moveTo(this.x + this.starLength, this.y);
                ctx.lineWidth = randomNumber(10);
                ctx.strokeStyle = randomColor();
                ctx.lineTo(this.x, this.y);
                ctx.stroke();
                this.x += this.dirX;
                this.y += this.dirY;
                if (this.x >= cW || this.y >= cH || this.x <= 0 || this.y <= 0 || this.starLength < 0) {
                    starArray.splice(starArray.indexOf(this), 1);
                }
            };
        };

        var starArray = [];
        var createStars = function (x, y) {
            numberOfStars = randomNumber(250) + 250;
            for (var i = 0; i < numberOfStars; i++) {
                starArray.push(new Star(x, y));
            }
        };

        var collision = false;
        var rocketExploded = false;

        var checkForCollision = function () {
            for (var i = 0, length = ufoArray.length; i < length; i++) {
                if (ufoArray[i].x + 60 >= rocketship.x && ufoArray[i].x <= rocketship.x + 150 && ufoArray[i].y - 10 <= rocketship.y + 25 && ufoArray[i].y + 20 >= rocketship.y) {
                    if (collision === false) {
                        createStars(rocketship.x, rocketship.y);
                        collision = true;
                    } else {
                        return;
                    }
                }
            }

            if (collision) {
                if (!rocketExploded) {
                    rocketExploded = true;
                    engineSound.pause();
                    laserSound.pause();
                    if (boomSound !== undefined) {
                        boomSound.play();
                    }
                }
                for (var i = 0; i < starArray.length; i++) {
                    starArray[i].starLength -= randomNumber(0.25);
                    starArray[i].draw();
                }
            }
        };

        var rocketActio = function () {
            if (!collision) {
                moveRocket();
                rocketship.render();
            } else {
                rocketship.x = -100;
                rocketship.y = -100;
            }
        };
        var level = 0;
        var checkUFOcount = function () {
            if (ufoArray.length === 0) {
                level += 2;
                createUFOs(level);
            }
        };

        var getScore = function () {
            return score;
        };

        var setScore = function () {
            ctx.beginPath();
            ctx.font = "400 56px Arial, sans-serif";
            ctx.textAlign = "start"; // start, end, left, right, center
            ctx.textBaseline = "top" // top, middle, bottom, hanging, alphabetic, ideographic
            ctx.strokeStyle = "red";
            ctx.fillStyle = "#FC0";
            ctx.fillText("UFO's killed: ", 100, 900);
            ctx.strokeText("UFO's killed: ", 100, 900);
            ctx.fillText(getScore(), 500, 900);
            ctx.strokeText(getScore(), 500, 900);
            ctx.fill();
            ctx.stroke();

        };

        var animate = function () {
            ctx.save();
            ctx.clearRect(0, 0, cW, cH);

            backgroundImage.render();
            sun.render();
            rocketActio();
            renderUFOs();
            rocketship.drift();
            checkForCollision();
            checkUFOcount();
            setScore();
            ctx.restore();
        };

        createUFOs();

        var speedUpUFOs = function () {
            for (var i = 0; i < ufoArray.length; i++) {
                ufoArray[i].speedUp();
            }
        };

        var animateInterval = setInterval(animate, 30);


        var addEvents = function () {
            rocketship.x = 100;
            rocketship.y = 100;

            window.onkeydown = window.onkeyup = function (e) {
                e = e || event; // to deal with IE
                map[e.keyCode] = e.type === 'keydown';
            }

            var pauseSound = function () {
                laserSound.pause();
                laserSound.currentTime = 0.0;
            };

            window.addEventListener('keyup', function (e) {
                switch (true) {
                    case e.keyCode === 40 || e.keyCode === 39 || e.keyCode === 38 || e.keyCode === 37:
                        engineSound.pause();
                        engineSound.currentTime = 0.0;
                        drifting = true;
                        break;
                    case e.keyCode === 32:
                        window.setTimeout(pauseSound, 200);
                        break;
                }
            });
        }();
    };


    window.addEventListener('load', function () {
        initCanvas();
    });

})();