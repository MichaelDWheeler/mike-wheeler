var express = require('express');
var mail = require('./dist/routes/mail');
var app = express();

app.use(express.static('dist'));
app.use(express.static('dist/html'));
app.use(express.static('dist/vendor'));
app.use(express.static('dist/css'));
app.use(express.static('dist/img'));
app.use(express.static('dist/js'));
app.use(express.static('dist/mp4'));
app.use(express.static('dist/routes'));

app.post('/mail', mail);
app.listen(process.env.PORT || 1337);
console.log('Port 1337 is listening');