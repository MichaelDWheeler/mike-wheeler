; (function () {

    const ctx = document.getElementById('my-canvas').getContext('2d');
    const soundArray = ['mp3/water-drop.mp3', 'mp3/water-drop-low.mp3'];

    ctx.canvas.width = $(window).width();
    ctx.canvas.height = $(window).height();

    const cW = ctx.canvas.width;
    const cH = ctx.canvas.height;

    const secondaryDropsArray = [];
    const SecondaryDrop = function (x, y, speed, radius) {
        this.speed = randomNumber(speed / 2);
        this.direction = randomNumber(this.speed) - this.speed / 2;
        this.x = x;
        this.y = y;
        this.radius = radius / randomNumber(5) + 2;
        this.reduceSpeed = function () {
            this.speed -= 0.10;
            if (this.speed >= speed) {
                this.speed = speed;
            }
            return this.speed;
        };

        this.explode = function () {
            this.y -= this.reduceSpeed(); //up
            this.x += this.direction;
            if (this.x < 0 || this.x > cW || this.y > cH + 20) {
                secondaryDropsArray.splice(secondaryDropsArray.indexOf(this), 1);
            }

            ctx.beginPath();
            ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
            ctx.lineWidth = 4;
            ctx.fillStyle = "aqua";
            ctx.shadowColor = '#00FFFF';
            ctx.shadowBlur = this.shadowBlur;
            ctx.stroke();
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.fillStyle = "#FFF";
            ctx.arc(this.x - this.radius / 2.5, this.y - this.radius / 2, this.radius / 2, 0, 2 * Math.PI, false);
            ctx.fill();
            ctx.closePath();
        }

    };

    const MainDrop = function (x, y, radius, speed, color) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.radius = radius;
        this.color = color;
        this.lineWidth = randomNumber(10) + 5;
        this.shadowBlur = randomNumber(10) + 10;

        this.createSecondaryDrops = function (num) {

            for (let i = 0; i < Math.round(num); i++) {
                secondaryDropsArray.push(new SecondaryDrop(this.x, this.y, this.speed, this.radius));
            }
        };

        this.addMainDrop = function () { // needs new dimensions to create a new drop
            let xPos = randomNumber(cW);
            let yPos = -randomNumber(cH);
            let radius = randomNumber(10) + 5;
            let speed = randomNumber(10) + 10;
            mainDropsArray.push(new MainDrop(xPos, yPos, radius, speed, "white"));
        };

        this.newDropsQty = function () {
            return randomNumber(3) + 2;
        };

        this.fall = function () {
            this.index = Math.floor(randomNumber(soundArray.length) - 1);
            this.y += speed;

            if (this.y >= cH) {

                let numberOfDrops = this.radius / randomNumber(3) + 2;
                const dropSound = new Audio(soundArray[this.index]);
                dropSound.play();
                mainDropsArray.splice(mainDropsArray.indexOf(this), 1);
                this.createSecondaryDrops(this.newDropsQty(), this.addMainDrop());
            }

            ctx.beginPath();
            ctx.arc(this.x, this.y, this.radius, 0, Math.PI, false);
            ctx.quadraticCurveTo(this.x - this.radius, this.y - this.radius / 2, this.x, this.y - this.radius * 2);
            ctx.quadraticCurveTo(this.x + this.radius, this.y - this.radius / 2, this.x + this.radius, this.y);
            ctx.closePath();
            ctx.strokeStyle = this.color;
            ctx.fillStyle = "aqua";
            ctx.lineWidth = this.lineWidth;
            ctx.shadowColor = '#00FFFF';
            ctx.shadowBlur = this.shadowBlur;
            ctx.stroke();
            ctx.fill();
            ctx.closePath();

            //highlight on drop
            ctx.beginPath();
            ctx.fillStyle = "#FFF";
            ctx.arc(this.x - this.radius / 2, this.y - this.radius / 2, this.radius / 2, 0, 2 * Math.PI, false);
            ctx.fill();
            ctx.closePath();


        };

    };

    const randomNumber = function (number) {
        let sum = Math.random() * number + 1;
        return sum;
    };

    let mainDropsArray = [];

    const createMainDrops = function () {
        for (let i = 0; i < randomNumber(10) + 10; i++) {
            let xPos = randomNumber(cW);
            let yPos = -randomNumber(cH);
            let radius = randomNumber(10) + 5;
            let speed = randomNumber(20) + 10;
            mainDropsArray.push(new MainDrop(xPos, yPos, radius, speed, "white"));
        }
    };

    const dropsFall = function () {
        for (let i = 0; i < mainDropsArray.length; i++) {
            mainDropsArray[i].fall();
        }
    };

    const secondaryDrops = function () {
        if (secondaryDropsArray.length > -1) {
            for (let i = 0; i < secondaryDropsArray.length; i++) {
                secondaryDropsArray[i].explode();
            }
        }
    };

    const animate = function () {
        ctx.clearRect(0, 0, cW, cH);
        ctx.save();

        dropsFall();
        secondaryDrops();

        ctx.restore();
        if (!stopAnimation) {
            window.requestAnimationFrame(animate);
        }

    };

    let stopAnimation = false;
    const addListeners = function () {
        window.addEventListener('click', function () {
            stopAnimation = !stopAnimation;
            animate();

        });
    };

    const thunderAudio = function () {
        let time = randomNumber(30) * 1000;
        setTimeout(function () {
            let thunderSound = new Audio();
            thunderSound.src = "mp3/thunder" + Math.floor(randomNumber(6)) + ".mp3";
            thunderSound.play();
            thunderAudio();
        }, time);
    };

    const rainAudio = function () {
        let rainSound = new Audio();
        rainSound.volume = "0.1";
        rainSound.src = "mp3/rain01.mp3";
        rainSound.play();
        setTimeout(function () {
            rainAudio();
        }, 2900);
    };

    const init = function () {
        addListeners();
        createMainDrops(window.requestAnimationFrame(animate));
        thunderAudio();
        rainAudio();
    };

    window.onload = init;

})();