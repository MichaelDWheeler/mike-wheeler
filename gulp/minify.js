(function () {
    'use strict';

    var gulp = require('gulp');
    var path = require('./config');
    var $ = require('gulp-load-plugins')({
        pattern: ['gulp-*', 'del']
    });

    gulp.task('clean', function (done) {
        return $.del([
            path.gulp.paths.dist + '/**', '!' + path.gulp.paths.dist, '!' + path.gulp.paths.dist + '/mailkey.js'
        ], done);
    });

    gulp.task('scripts', function () {
        return gulp.src(path.gulp.paths.src + '/js/*.js')
            .pipe($.concat('bundle.js'))
            .pipe($.browserify())
            .pipe($.uglify())
            .pipe(gulp.dest(path.gulp.paths.dist + '/js/'));
    });

    gulp.task('html', function () {
        return gulp.src(path.gulp.paths.src + '/html/*.html')
            .pipe($.minifyHtml())
            .pipe(gulp.dest(path.gulp.paths.dist + '/html/'));
    });

    gulp.task('css', function () {
        return gulp.src(path.gulp.paths.src + '/css/*.css')
            .pipe($.concat('style.css'))
            .pipe($.cleanCss())
            .pipe(gulp.dest(path.gulp.paths.dist + '/css/'));
    });

    gulp.task('move', function () {
        return gulp.src(path.gulp.paths.src + '/vendor/**/*.*')
            .pipe(gulp.dest(path.gulp.paths.dist + '/vendor/'));
    });

    gulp.task('mail', function () {
        return gulp.src(path.gulp.paths.src + '/routes/**/*.*')
            .pipe(gulp.dest(path.gulp.paths.dist + '/routes/'));
    });

    gulp.task('images', function () {
        return gulp.src(path.gulp.paths.src + '/img/*.*')
            .pipe($.imagemin())
            .pipe(gulp.dest(path.gulp.paths.dist + '/img/'));
    });

    gulp.task('mp4', function () {
        return gulp.src(path.gulp.paths.src + '/mp4/*.*')
            .pipe(gulp.dest(path.gulp.paths.dist + '/mp4/'));
    });

 

    gulp.task('watch', function () {
        gulp.watch(path.gulp.paths.src + '/html/*.html', ['html']);
        gulp.watch(path.gulp.paths.src + '/css/*.css', ['css']);
        gulp.watch(path.gulp.paths.src + '/js/*.js', ['scripts']);
        gulp.watch(path.gulp.paths.src + '/img/**', ['images']);
        gulp.watch(path.gulp.paths.src + '/vendor/**', ['move']);
    });


    gulp.task('buildapp', ['scripts', 'html', 'css', 'move', 'images', 'mp4', 'watch', 'mail'], function () {
        gulp.start('nodemon');
    });

    gulp.task('build', ['clean'], function () {
        gulp.start('buildapp');
    });

    console.log('Finished gulp tasks');

})();